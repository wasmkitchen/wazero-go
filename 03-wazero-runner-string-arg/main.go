package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

func main() {
	// Choose the context to use for function calls.
	ctx := context.Background()

	// Create a new WebAssembly Runtime.
	//wasmRuntime := wazero.NewRuntime(ctx)
	wasmRuntime := wazero.NewRuntimeWithConfig(ctx, wazero.NewRuntimeConfigInterpreter())

	defer wasmRuntime.Close(ctx) // This closes everything this Runtime created.

	/*
		Package wasi_snapshot_preview1 contains Go-defined functions to access system calls,
		such as opening a file, similar to Go's x/sys package.
		These are accessible from WebAssembly-defined functions via importing ModuleName.
		All WASI functions return a single Errno result: ErrnoSuccess on success.
	*/
	wasi_snapshot_preview1.MustInstantiate(ctx, wasmRuntime)

	// Load the WebAssembly module
	wasmFilePath := "../functions/03-say-hello/say-hello.wasm"
	helloWasmModule, err := os.ReadFile(wasmFilePath)
	if err != nil {
		log.Panicln(err)
	}

	// Instantiate the WebAssembly module
	mod, err := wasmRuntime.InstantiateModuleFromBinary(ctx, helloWasmModule)
	if err != nil {
		log.Panicln(err)
	}

	// ======================================================
	// Pass a string param and get a string result
	// ======================================================
	// Let's use the argument to this main function in Wasm.
	name := "Bob Morane"
	nameSize := uint64(len(name))
	// Get references to WebAssembly functions we'll use in this example.

	sayHelloWasmFunction := mod.ExportedFunction("sayHello")

	// These are undocumented, but exported. See tinygo-org/tinygo#2788
	malloc := mod.ExportedFunction("malloc")
	free := mod.ExportedFunction("free")

	// Instead of an arbitrary memory offset, use TinyGo's allocator. Notice
	// there is nothing string-specific in this allocation function. The same
	// function could be used to pass binary serialized data to Wasm.

	// book some place in memory for the parameter
	results, err := malloc.Call(ctx, nameSize)
	if err != nil {
		log.Panicln(err)
	}
	namePtr := results[0]
	// This pointer is managed by TinyGo, but TinyGo is unaware of external usage.
	// So, we have to free it when finished
	defer free.Call(ctx, namePtr)

	// The pointer is a linear memory offset, which is where we write the name.

	// write the name parameter in memory
	ok := mod.Memory().Write(ctx, uint32(namePtr), []byte(name))
	if !ok {
		log.Panicln("😡 Houston, We've Got a Problem")
	}

	// Finally, we get the greeting message "greet" printed. This shows how to
	// read-back something allocated by TinyGo.
	posAndLengthReturnValue, err := sayHelloWasmFunction.Call(ctx, namePtr, nameSize)
	if err != nil {
		log.Panicln(err)
	}
	// Note: This pointer is still owned by TinyGo, so don't try to free it!
	stringPos := uint32(posAndLengthReturnValue[0] >> 32)
	stringLength := uint32(posAndLengthReturnValue[0])
	// The pointer is a linear memory offset, which is where we write the name.
	bytes, ok := mod.Memory().Read(ctx, stringPos, stringLength)
	if !ok {
		log.Panicln("😡 Houston, We've Got a Problem")
	} else {
		fmt.Println("😃 message from WASM:", string(bytes))
	}

}

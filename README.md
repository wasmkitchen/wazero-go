# wazero-go

# Call a WASM Go (TinyGo) function from Go (with Wazero)
> `func add(x int, y int) int`

- Wazero launcher: `01-wazero-runner-simple`
- WASM module: `/functions/01-add/add.wasm`

# Call a WASM function that returns a string

- Wazero launcher: `/02-wazero-runner-get-string`
- WASM module: `/functions/02-hello-world/hello-world.wasm`

# Call a WASM function with a string parameter that returns a string

- Wazero launcher: `/03-wazero-runner-string-arg`
- WASM module: `/functions/03-say-hello/say-hello.wasm`

# Create a host function to display a string

- Wazero launcher: `/04-wazero-runner-host-function`
- WASM module: `/functions/04-add-again/add-again.wasm`

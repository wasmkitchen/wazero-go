package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/api"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

func main() {
	// Choose the context to use for function calls.
	ctx := context.Background()

	// Create a new WebAssembly Runtime.
	//wasmRuntime := wazero.NewRuntime(ctx)
	wasmRuntime := wazero.NewRuntimeWithConfig(ctx, wazero.NewRuntimeConfigInterpreter())

	defer wasmRuntime.Close(ctx) // This closes everything this Runtime created.

	logString := api.GoModuleFunc(func(ctx context.Context, module api.Module, params []uint64) {
		//fmt.Println("🌺", params)
		//fmt.Println("🖐 position:", params[0])
		//fmt.Println("🖐 length:", params[1])

		position := uint32(params[0]) // <-- this comes from the wasm function
		length := uint32(params[1])

		buffer, ok := module.Memory().Read(ctx, position, length)
		if !ok {
			log.Panicln("😡 Houston, We've Got a Problem")
		}
		fmt.Println("🐼:", string(buffer))

		params[0] = 0
		//return nil
	})

	getEnvVar := api.GoModuleFunc(func(ctx context.Context, module api.Module, params []uint64) {

		position := uint32(params[0]) // <-- this comes from the wasm function (variable name)
		length := uint32(params[1])

		buffer, ok := module.Memory().Read(ctx, position, length)
		if !ok {
			log.Panicln("😡 Houston, We've Got a Problem")
		}
		variableName := string(buffer)
		fmt.Println("[Host] variable name:", variableName)

		variableValue := os.Getenv(variableName)

		variableValueLength := len(variableValue)

		// this is a wasm function
		results, err := module.ExportedFunction("allocateBuffer").Call(ctx, uint64(variableValueLength))
		if err != nil {
			log.Panicln("😡 Houston, We've Got a Problem", err)
		}

		fmt.Println("[Host] variable value:", variableValue)

		positionReturnBuffer := uint32(params[2])
		lengthReturnBuffer := uint32(params[3])

		allocatedPosition := uint32(results[0])
		module.Memory().WriteUint32Le(ctx, positionReturnBuffer, allocatedPosition)
		module.Memory().WriteUint32Le(ctx, lengthReturnBuffer, uint32(variableValueLength))

		// add the message to the memory of the module
		module.Memory().Write(ctx, allocatedPosition, []byte(variableValue))

		params[0] = 0

	})

	builder := wasmRuntime.NewHostModuleBuilder("env")

	builder.NewFunctionBuilder().
		WithGoModuleFunction(logString,
			[]api.ValueType{
				api.ValueTypeI32, // position
				api.ValueTypeI32, // length
			},
			[]api.ValueType{api.ValueTypeI32}).
		Export("hostLogString")

	builder.NewFunctionBuilder().
		WithGoModuleFunction(getEnvVar,
			[]api.ValueType{
				api.ValueTypeI32, // position (variable name)
				api.ValueTypeI32, // length
				api.ValueTypeI32, // position (returned value)
				api.ValueTypeI32, // length
			},
			[]api.ValueType{api.ValueTypeI32}).
		Export("hostGetEnvVar")

	_, errBuilder := builder.Instantiate(ctx, wasmRuntime)
	if errBuilder != nil {
		log.Panicln("😡 Houston, We've Got a Problem", errBuilder)
	}

	/*
		Package wasi_snapshot_preview1 contains Go-defined functions to access system calls,
		such as opening a file, similar to Go's x/sys package.
		These are accessible from WebAssembly-defined functions via importing ModuleName.
		All WASI functions return a single Errno result: ErrnoSuccess on success.
	*/
	wasi_snapshot_preview1.MustInstantiate(ctx, wasmRuntime)

	// Load the WebAssembly module
	wasmFilePath := "../functions/05-add-again-and-again/add-again-and-again.wasm"
	addWasmModule, err := os.ReadFile(wasmFilePath)
	if err != nil {
		log.Panicln(err)
	}

	// Instantiate the WebAssembly module
	mod, err := wasmRuntime.InstantiateModuleFromBinary(ctx, addWasmModule)
	if err != nil {
		log.Panicln(err)
	}

	// Get references to WebAssembly function: "add"
	addWasmFunction := mod.ExportedFunction("add")

	// Now, we can call "add", which reads the data we wrote to memory!
	// result []uint64
	result, err := addWasmFunction.Call(ctx, 20, 22)
	if err != nil {
		log.Panicln(err)
	}

	fmt.Println("result:", result[0])
}

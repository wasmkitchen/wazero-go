package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

func main() {
	// Choose the context to use for function calls.
	ctx := context.Background()

	// Create a new WebAssembly Runtime.
	//wasmRuntime := wazero.NewRuntime(ctx)
	wasmRuntime := wazero.NewRuntimeWithConfig(ctx, wazero.NewRuntimeConfigInterpreter())

	defer wasmRuntime.Close(ctx) // This closes everything this Runtime created.

	/*
		Package wasi_snapshot_preview1 contains Go-defined functions to access system calls,
		such as opening a file, similar to Go's x/sys package.
		These are accessible from WebAssembly-defined functions via importing ModuleName.
		All WASI functions return a single Errno result: ErrnoSuccess on success.
	*/
	wasi_snapshot_preview1.MustInstantiate(ctx, wasmRuntime)

	// Load the WebAssembly module
	wasmFilePath := "../functions/02-hello-world/hello-world.wasm"
	helloWasmModule, err := os.ReadFile(wasmFilePath)
	if err != nil {
		log.Panicln(err)
	}

	// Instantiate the WebAssembly module
	mod, err := wasmRuntime.InstantiateModuleFromBinary(ctx, helloWasmModule)
	if err != nil {
		log.Panicln(err)
	}

	// ======================================================
	// Get a string from wasm
	// ======================================================
	helloWasmFunction := mod.ExportedFunction("helloWorld")

	// Call the Wasm
	posAndLength, errCallFunction := helloWasmFunction.Call(ctx)
	if errCallFunction != nil {
		log.Panicln("🔴 Error while calling the function ", errCallFunction)
	}

	// Extract Position and Length
	stringPos := uint32(posAndLength[0] >> 32)
	stringLength := uint32(posAndLength[0])

	// The pointer is a linear memory offset, which is where we write the name.
	bytes, ok := mod.Memory().Read(ctx, stringPos, stringLength)
	if !ok {
		log.Panicln("😡 Houston, We've Got a Problem")
	} else {
		fmt.Println("😃 the string message is:", string(bytes))
	}

}

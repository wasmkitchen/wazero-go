package main

import (
	"reflect"
	"unsafe"
)

// stringToPtr returns a pointer (position in memory) and
// size (length of the string) pair for the given string
// in a way compatible with WebAssembly numeric types.
func stringToPtr(s string) (uint32, uint32) {
	buf := []byte(s)
	ptr := &buf[0]
	unsafePtr := uintptr(unsafe.Pointer(ptr))
	return uint32(unsafePtr), uint32(len(buf))
}

// ptrToString returns a string from WebAssembly compatible numeric types
// representing its pointer and length.

func ptrToString(ptr uint32, size uint32) string {
	// Get a slice view of the underlying bytes in the stream. We use SliceHeader, not StringHeader
	// as it allows us to fix the capacity to what was allocated.
	return *(*string)(unsafe.Pointer(&reflect.SliceHeader{
		Data: uintptr(ptr),
		Len:  uintptr(size), // Tinygo requires these as uintptrs even if they are int fields.
		Cap:  uintptr(size), // ^^ See https://github.com/tinygo-org/tinygo/issues/1284
	}))
}

func main() {

}

//export sayHello
func sayHello(ptr, size uint32) (ptrAndSize uint64) {
	// get the parameter
	name := ptrToString(ptr, size)

	ptr, size = stringToPtr("👋 hello " + name + " 😃")
	return (uint64(ptr) << uint64(32)) | uint64(size)
}

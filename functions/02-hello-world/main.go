package main

import "unsafe"

// stringToPtr returns a pointer (position in memory) and
// size (length of the string) pair for the given string
// in a way compatible with WebAssembly numeric types.
func stringToPtr(s string) (uint32, uint32) {
	buf := []byte(s)
	ptr := &buf[0]
	unsafePtr := uintptr(unsafe.Pointer(ptr))
	return uint32(unsafePtr), uint32(len(buf))
}

func main() {

}

// 🖐️ returns a pointer/size pair packed into an uint64.
// Note: This uses an uint64 instead of two result values for compatibility with
// WebAssembly 1.0.

//export helloWorld
func helloWorld() (ptrAndSize uint64) {
	ptr, size := stringToPtr("👋 hello world 🌍")
	return (uint64(ptr) << uint64(32)) | uint64(size)
}

// ptr is the position of the string in memory

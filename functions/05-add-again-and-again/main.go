package main

import (
	"reflect"
	"unsafe"
)

//export allocateBuffer
//go:linkname allocateBuffer
func allocateBuffer(size uint32) *byte {
	// Allocate the in-Wasm memory region and returns its pointer to hosts.
	// The region is supposed to store random strings generated in hosts,
	// meaning that this is called "inside" of host_get_string.
	buf := make([]byte, size)
	return &buf[0]
}

// stringToPtr returns a pointer (position in memory) and
// size (length of the string) pair for the given string
// in a way compatible with WebAssembly numeric types.
func stringToPtr(s string) (uint32, uint32) {
	buf := []byte(s)
	ptr := &buf[0]
	unsafePtr := uintptr(unsafe.Pointer(ptr))
	return uint32(unsafePtr), uint32(len(buf))
}

// ptrToString returns a string from WebAssembly compatible numeric types
// representing its pointer and length.

func ptrToString(ptr uint32, size uint32) string {
	// Get a slice view of the underlying bytes in the stream. We use SliceHeader, not StringHeader
	// as it allows us to fix the capacity to what was allocated.
	return *(*string)(unsafe.Pointer(&reflect.SliceHeader{
		Data: uintptr(ptr),
		Len:  uintptr(size), // Tinygo requires these as uintptrs even if they are int fields.
		Cap:  uintptr(size), // ^^ See https://github.com/tinygo-org/tinygo/issues/1284
	}))
}

func getStringResult(buffPtr *byte, buffSize int) string {

	result := *(*string)(unsafe.Pointer(&reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(buffPtr)),
		Len:  uintptr(buffSize),
		Cap:  uintptr(buffSize),
	}))

	//Log("🟧 getStringResult: " + result)

	return result
}

//export hostLogString
//go:linkname hostLogString
func hostLogString(position, length uint32) uint32

//export hostGetEnvVar
//go:linkname hostGetEnvVar
func hostGetEnvVar(variableNamePosition, variableNameLength uint32, returnValuePosition **byte, returnValueLength *int) uint32

func main() {
	//fmt.Println("Hello from Go")
	pos, length := stringToPtr("Hello from the main function")
	hostLogString(pos, length)

}

// This exports an add function.
// It takes in two 32-bit integer values
// And returns a 32-bit integer value.
// To make this function callable from host,
// we need to add the: "export add" comment above the function

//export add
func add(x int, y int) int {
	pos, length := stringToPtr("Hello from the add function")
	hostLogString(pos, length)

	var buffPtr *byte
	var buffSize int
	posVar, lengthVar := stringToPtr("MESSAGE")
	hostGetEnvVar(posVar, lengthVar, &buffPtr, &buffSize)

	valueOfVariable := getStringResult(buffPtr, buffSize) // <> ptrToString

	hostLogString(stringToPtr("[WASM Module] " + valueOfVariable))

	return x + y
}

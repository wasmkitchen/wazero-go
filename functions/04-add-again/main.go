package main

import "unsafe"

// stringToPtr returns a pointer (position in memory) and 
// size (length of the string) pair for the given string 
// in a way compatible with WebAssembly numeric types.
func stringToPtr(s string) (uint32, uint32) {
	buf := []byte(s)
	ptr := &buf[0]
	unsafePtr := uintptr(unsafe.Pointer(ptr))
	return uint32(unsafePtr), uint32(len(buf))
}

//export hostLogString
//go:linkname hostLogString
func hostLogString(position, length uint32) uint32

func main() {
	//fmt.Println("Hello from Go")
	pos, length := stringToPtr("Hello from the main function")
	hostLogString(pos, length)

}

// This exports an add function.
// It takes in two 32-bit integer values
// And returns a 32-bit integer value.
// To make this function callable from host,
// we need to add the: "export add" comment above the function

//export add
func add(x int, y int) int {
	pos, length := stringToPtr("Hello from the add function")
	hostLogString(pos, length)

	return x + y;
}
